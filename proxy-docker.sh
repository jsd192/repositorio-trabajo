#!/bin/bash

systemctl stop firewalld
setenforce 0
mkdir -p prueba-jenrry
cd prueba-jenrry

cat > nginx.conf << EOF
http {
	server {
		listen 80;
		root /var/www/virtualhost/;
	}
}

events { }
EOF

cat > Dockerfile << EOF
FROM nginx
COPY nginx.conf /etc/nginx/nginx.conf
EOF

docker build -t custom-nginx .
docker run -v /var/www/virtualhost:/var/www/virtualhost:ro --name proxy_nginx -dp 80:80 custom-nginx

mkdir -p /var/www/virtualhost/dextre.com
mkdir -p /var/www/virtualhost/soto.com

cat > index.html << EOF
Hola este ese el sitio web de Dextre.com
EOF

mv index.html /var/www/virtualhost/dextre.com/

cat > index.html << EOF
Hola este ese el sitio web de Soto.com
EOF

mv index.html /var/www/virtualhost/soto.com/

mipe=`hostname -I | cut -d" " -f1`
echo " Porfavor abrir su navegador y poner"
echo -e " $mipe/dextre.com \n $mipe/soto.com "



