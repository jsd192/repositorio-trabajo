#!/bin/bash

echo 's' | yum install epel-release -y

yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine

echo 's' | yum install yum-utils unzip zip vim -y

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
echo 's' | yum update -y
yum install docker-ce -y
yum install docker-compose -y
systemctl enable --now docker containerd


systemctl start docker

systemctl status docker

systemctl enable docker

mkdir -p  /root/docker_ejemplo
unzip liquorstore-master.zip && mv liquorstore-master /root/docker_ejemplo/


cat > Dockerfile << "EOF"
FROM nginx:1.19.0-alpine

COPY liquorstore-master/ /usr/share/nginx/html

EOF

mv Dockerfile /root/docker_ejemplo/

cd /root/docker_ejemplo/ && docker build -t new-nginx .
docker run -v ~/docker_ejemplo/liquorstore-master:/usr/share/nginx/html:ro -dp 80:80 new-nginx

docker ps -a
